package tp4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.InetAddress;
import java.util.Scanner;
/**
 *
 * @author agiavari
 */
public class Client {
    
    public static void main(String [] args){
        Socket socket;
        BufferedReader reception;
        PrintWriter send;
        Scanner sc = new Scanner(System.in);
        
        try{
            socket = new Socket(InetAddress.getLocalHost(),2009);

            /*Preparation flux sortant et entrant*/
            send = new PrintWriter(socket.getOutputStream());
            reception = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                       
            while(true){
                System.out.println("Message à saisir :");
                String ligne = sc.nextLine();
                send.println(ligne); //Le client envoie un message au serveur
                send.flush(); //Pour vider le buffer
                
                String lectureMessage = reception.readLine(); //puis lit sa reponse
                System.out.println(lectureMessage);
            }
                              
            //socket.close();
            
        }catch (IOException e){e.printStackTrace();}
    }
}
