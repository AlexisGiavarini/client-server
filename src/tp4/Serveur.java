package tp4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author agiavari
 */
public class Serveur {

    public static void main(String[] args) {
        ServerSocket serverSocket;
        Socket socket;
        PrintWriter send;
        BufferedReader reception;

        try {
            serverSocket = new ServerSocket(2009); //Socket sur le port 2009
            System.out.println("En attente d'une connection...");
            socket = serverSocket.accept(); //Attend la connexion du client
            reception = new BufferedReader(new InputStreamReader(socket.getInputStream())); //Flux entrant (reception)
            send = new PrintWriter(socket.getOutputStream()); //Permet l'envoie de chaines de caracteres (flux sortant)

            /*Traitement*/
            int compteur = 0;
            System.out.println("Client connecté");

            while (true) {
                System.out.println("Le client saisie un message");
                String lectureMessage = reception.readLine(); //Le serveur reçoit le message du client
                System.out.println("Message du client : " + lectureMessage);// et le lit
                
                if(lectureMessage.equalsIgnoreCase("finir")){ //Si le client saisie "finir"
                    serverSocket.close();                    // On ferme le serveur
                    socket.close();
                }else{
                    send.println(lectureMessage+" " + InetAddress.getLocalHost() + " " + compteur); //Puis il repond au client
                    send.flush(); // Pour vider le buffer
                    compteur++;
                }               
            }            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
